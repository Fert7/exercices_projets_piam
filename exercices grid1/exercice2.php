<html>
    <?php include('main.php'); ?>
    <body>
    <h1 class="font-bold text-2xl p-4 text-center ">Exercice 2</h1>
    <p class="p-4 text-center">Navbar fixed</p>  
            <div class="grid grid-cols-1 justify-items-center">
                <button class="bg-gray-700 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"> <a href="exercices3.php"> Suivant </a></button>
            </div>
              
            <br>

            <nav class="fixed inset-x-0 flex justify-between bg-gray-700 m-6 py-4 px-4 text-white">
                <div class="hover:bg-gray-200 text-center w-full">
                    <a href="#"class="block " > Accueil</a>
                </div>
                <div class="hover:bg-gray-200 text-center w-full">
                    <a href="#"class="block "> A propos</a>
                </div>
                <div class="hover:bg-gray-200 text-center w-full">  
                    <a href="#"class="block "> Services</a>
                </div>
                <div class="hover:bg-gray-200 text-center w-full">  
                    <a href="#"class="block "> Contact</a>
                </div>

            </nav>
            <div class="min-h-screen bg-gray-200 grid grid-cols-2 h-1/2 text-justify rounded-2xl">
                <p class=" bg-red-200 p-14">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more 
                recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>
                <p class="bg-gray-300 p-14">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more
                 recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>

            </div> 
            <!-- This is an example component -->
             
    <?php include('script.php'); ?>
    </body>
</html>
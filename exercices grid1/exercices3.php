<html>
        <?php include('main.php'); ?>
    <body>
        <h1 class="font-bold text-2xl p-4 text-center ">Exercice 4</h1>
        <p class="p-4 text-center">Sticky Elements</p> 
        <div class="p-6 ">
            <div class="flex space-x-2 p-4 ">
                <div class="w-1/3 col-sm-6">
                    <h1 class="font-bold text-2xl font-mono top-10 md:flex md:sticky ">
                        Introduction
                    </h1>
                </div>
                
                <div class="  w-2/3 col-sm-6 leading-800 bg-gray-200 hover:bg-blue-200 rounded-2xl p-4 space-y-4 md:block">
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>

                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
            </div>


            <div class="flex space-x-2 p-4">
                <div class="w-1/3">
                    <h1 class="font-bold text-2xl font-mono sticky top-10">
                    Développement
                    </h1>
                </div>
                
                <div class="w-2/3 leading-800 bg-gray-200 hover:bg-blue-200 rounded-2xl p-4 space-y-4">
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>

                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
            </div>

            <div class="flex space-x-2 p-4">
                <div class="w-1/3">
                    <h1 class="font-bold text-2xl font-mono sticky top-10">
                        Etudes de cas
                    </h1>
                </div>
                
                <div class="w-2/3 leading-800 bg-gray-200 hover:bg-blue-200 rounded-2xl p-4 space-y-4">
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>

                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
            </div>

            <div class="flex space-x-2 p-4">
                <div class="w-1/3">
                    <h1 class="font-bold text-2xl font-mono sticky top-10">
                        Conclusion et solution
                    </h1>
                </div>
                
                <div class="w-2/3 leading-800 bg-gray-200 hover:bg-blue-200 rounded-2xl p-4 space-y-4">
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>

                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions 
                        have ev olved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                    </p>
                </div>
            </div>
        </div>
        <?php include('script.php'); ?>  
    </body>

</html>
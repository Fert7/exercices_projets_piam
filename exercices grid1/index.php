<html>
    <?php include('main.php'); ?>
    <body>
        <h1 class="font-bold text-2xl p-4 text-center ">Exercice 1</h1>
        <p class="p-4 text-center">Disposition de layout suivant GRID</p>
        <div class="grid grid-cols-1 md:grid-rows-2 text-center content-end gap-6 mt-4 p-4 ">
            <p class="bg-blue-200">grid1</p>
            <div class="grid grid-cols-2 gap-6">
                <p class="bg-blue-200">grid2</p>
                <div class="grid grid-cols-1 gap-6">
                    <p class="bg-red-200">grid3</p>
                    <div class="grid grid-cols-3 gap-6">
                    <p class="bg-green-200">grid 5</p>  
                    <p class="bg-green-200">grid 6</p> 
                    <p class="bg-green-200">grid 7</p>             
                    </div>
                </div>
            </div>   
        </div>
        <div class="grid grid-cols-1 justify-items-center">
            <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"> <a href="exercice2.php"> Suivant </a></button>
        </div>
    <?php include('script.php'); ?>
    </body>

</html>
